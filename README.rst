Virtual-IPM
===========

Virtual-IPM is a software which combines efforts towards simulations of Ionization Profile Monitors
and was developed for the IPMSim collaboration. It has been designed with focus on flexibility and
extensibility in order to cover a broad range of
`use cases <https://ipmsim.gitlab.io/Virtual-IPM/use-cases.html>`_.
It combines aspects of a framework
- which enables `developers <https://ipmsim.gitlab.io/Virtual-IPM/developers/introduction.html>`_
to realize their own ideas - as well as of a stand-alone application which allows
`users <https://ipmsim.gitlab.io/Virtual-IPM/usage.html>`_ to effortlessly specify and run their own cases.


Relevant links
--------------

* `Documentation <https://ipmsim.gitlab.io/Virtual-IPM/>`_
* `Changelog / Release notes <https://ipmsim.gitlab.io/Virtual-IPM/changelog.html>`_
* `Examples <https://ipmsim.gitlab.io/Virtual-IPM/examples.html>`_
* `Virtual-IPM on PyPI <https://pypi.org/project/virtual-ipm/>`_
