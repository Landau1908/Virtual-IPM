# -*- coding: utf-8 -*-

#    Virtual-IPM is a software for simulating IPMs and other related devices.
#    Copyright (C) 2017  The IPMSim collaboration <http://ipmsim.gitlab.io/IPMSim>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import absolute_import, unicode_literals

import argparse
import logging
import pprint
import re
import six
import time

from anna import load_from_file
import pyhocon
from rx.concurrency import current_thread_scheduler
import telepot

from virtual_ipm.control.threading import SimulationThread
import virtual_ipm.log as log

parser = argparse.ArgumentParser()
parser.add_argument('-t', required=True, help='Telegram configuration file path')
parser.add_argument('-c', help='Configuration file path')
args = parser.parse_args()

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter('[%(levelname)s] %(message)s'))
handler.setLevel(logging.DEBUG)
logger.addHandler(handler)


class InvalidConfigurationPathError(Exception):
    pass


class MessageHandler(object):
    def __init__(self, bot, telegram_config, simulation_config_name=None):
        self._bot = bot
        self._telegram_config = telegram_config
        self._client_chat_ids = [int(telegram_config['bot.chat_id'])]
        self._simulation_config_name = simulation_config_name
        self._thread = None

    def __call__(self, message):
        content_type, chat_type, chat_id = telepot.glance(message)
        if chat_id not in self._client_chat_ids:
            logger.info(
                'Received a message from a non-registered client: %s'
                % pprint.pformat(message)
            )
            return
        logger.debug('Got message of type %s' % content_type)
        if content_type == 'text':
            logger.debug('Got text message: %s' % message['text'])
            match = re.match(r'^/([a-z]+)( [./_\-a-zA-Z0-9\s]+)?$', message['text'])
            if match is not None:
                command, arguments = match.groups()
                arguments = arguments.strip().split() if arguments is not None else []
                logger.debug('Dispatching to command handler: %s, %s' % (command, arguments))
                try:
                    getattr(self, 'cmd_%s' % command)(*arguments)
                except AttributeError:
                    self.react_unknown_command(message, command)
                except TypeError as err:
                    logger.error(six.text_type(err) + six.text_type(arguments))
                    self.react_invalid_number_of_arguments(message)
                except InvalidConfigurationPathError as err:
                    self.response(message, six.text_type(err))

    def cmd_config(self, config_filename):
        logger.debug('Using configuration filename "%s"' % config_filename)
        self._simulation_config_name = config_filename

    def cmd_start(self):
        if self._simulation_config_name is None:
            raise InvalidConfigurationPathError(
                'You need to specify a configuration file first using /config'
            )
        log.to_console(level=logging.INFO)
        self._thread = SimulationThread()

        self._thread.progress.observe_on(current_thread_scheduler)\
            .map(lambda x: int(float(x.step) / x.max_steps * 100))\
            .filter(lambda x: x % 10 == 0)\
            .distinct_until_changed()\
            .subscribe(
                lambda percentage: self.send_simulation_update(
                    r'{0}% completed'.format(percentage)
                )
            )

        log_handler = log.SubjectHandler(format_string='%(message)s')
        log_handler.records.observe_on(current_thread_scheduler)\
            .filter(lambda x: re.match(r'^Completed step \d+$', x) is None)\
            .subscribe(lambda x: self.send_simulation_update(x))
        log.add_handler(log_handler)

        self._thread.setup(load_from_file(self._simulation_config_name))
        self._thread.start()

    def react_invalid_number_of_arguments(self, message):
        self.response(message, 'Invalid number of arguments')

    def react_unknown_command(self, message, command):
        self.response(message, 'Unknown command "%s"' % command)

    def response(self, message, text):
        content_type, chat_type, chat_id = telepot.glance(message)
        self._bot.sendMessage(chat_id, text)

    def send_message(self, chat_id, text):
        self._bot.sendMessage(chat_id, text)

    def send_simulation_update(self, text):
        for chat_id in self._client_chat_ids:
            self.send_message(chat_id, text)


config = pyhocon.ConfigFactory.parse_file(args.t)

bot = telepot.Bot(config['bot.api_key'])
try:
    bot.message_loop(MessageHandler(bot, config, args.c))
except AttributeError as err:
    if six.text_type(err) == 'c':
        bot.message_loop(MessageHandler(bot, config))
    else:
        raise

while True:
    time.sleep(1)
