# -*- coding: utf-8 -*-

#    Virtual-IPM is a software for simulating IPMs and other related devices.
#    Copyright (C) 2017  The IPMSim collaboration <http://ipmsim.gitlab.io/IPMSim>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import absolute_import, unicode_literals

from anna import ActionParameter, BoolParameter, IntegerParameter, \
    NumberParameter, StringParameter, VectorParameter
from anna.parameters import Parameter
import flask
from flask import abort, Flask, jsonify

import virtual_ipm.simulation.devices as devices
import virtual_ipm.simulation.output as output
import virtual_ipm.simulation.particle_generation.models as particle_generation
import virtual_ipm.simulation.particle_tracking.models as particle_tracking
import virtual_ipm.simulation.particle_tracking.em_fields.guiding_fields.models.electric as \
    electric_guiding_fields
import virtual_ipm.simulation.particle_tracking.em_fields.guiding_fields.models.magnetic as \
    magnetic_guiding_fields


app = Flask('vipm')

# configuration = JSONConfiguration()

_interfaced_components = (
    devices,
    electric_guiding_fields,
    magnetic_guiding_fields,
    output,
    particle_generation,
    particle_tracking
)

_parameter_converters = {}


def dispatches_from_parameter(parameter_cls):
    def decorator(func):
        _parameter_converters[parameter_cls] = func
        return func
    return decorator


@dispatches_from_parameter(BoolParameter)
def convert_bool_parameter_to_json(parameter):
    return {}


@dispatches_from_parameter(IntegerParameter)
def convert_integer_parameter_to_json(parameter):
    return {}


@dispatches_from_parameter(NumberParameter)
def convert_number_parameter_to_json(parameter):
    return {'unit': parameter['unit']}


@dispatches_from_parameter(StringParameter)
def convert_string_parameter_to_json(parameter):
    return {}


@dispatches_from_parameter(VectorParameter)
def convert_vector_parameter_to_json(parameter):
    return {'unit': parameter['unit']}


def convert_parameter_to_json(parameter):
    if parameter.__class__ is ActionParameter:
        parameter_cls = parameter.parameter.__class__
        parameter_type = parameter.parameter.__class__.__name__
    else:
        parameter_cls = parameter.__class__
        parameter_type = parameter.__class__.__name__
    return dict(_parameter_converters[parameter_cls](parameter),
                default=parameter.default if 'default' in parameter else None,
                expert=parameter.is_expert,
                path=parameter.path,
                type=parameter_type)


def retrieve_interfaced_components(module):
    interface = module.Interface
    return filter(lambda c: c is not interface,
                  filter(lambda obj: isinstance(obj, type) and issubclass(obj, interface),
                         map(lambda obj_str: getattr(module, obj_str), dir(module))))


def retrieve_interfaced_components_by_name(module):
    return map(lambda c: c.__name__, retrieve_interfaced_components(module))


def parametrized_component_view(component):
    parameters = filter(lambda obj: isinstance(obj, Parameter),
                        map(lambda obj_str: getattr(component, obj_str), dir(component)))
    attributes = dict(doc=component.__doc__, name=component.__name__, parameters=[])
    for parameter in parameters:
        attributes['parameters'].append(convert_parameter_to_json(parameter))
    return {
        component.__name__: attributes
    }


@app.route('/config/<path:path>', methods=['GET'])
def get_config(path):
    pass


@app.route('/config/<path:path>', methods=['PUT'])
def set_config(path):
    pass


@app.route('/components', methods=['GET'])
def components_index():
    response = flask.make_response(
        jsonify({
            'components': [
                'devices',
                'electric_guiding_fields',
                'magnetic_guiding_fields',
                'output',
                'particle_generation',
                'particle_tracking',
                'simulation',
            ]
        })
    )
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response


@app.route('/components/<component_str>', methods=['GET'])
def specific_component_index(component_str):
    try:
        component = globals()[component_str]
    except KeyError:
        abort(404)
    else:
        if component in _interfaced_components:
            response = flask.make_response(jsonify({
                component_str: retrieve_interfaced_components_by_name(component)
            }))
        else:
            response = flask.make_response(jsonify({
                component_str: parametrized_component_view(component)
            }))
        response.headers['Access-Control-Allow-Origin'] = '*'
        return response


@app.route('/components/<component>/<cls>', methods=['GET'])
def get_specific_component(component, cls):
    try:
        cls_obj = getattr(globals()[component], cls)
    except (AttributeError, KeyError):
        abort(404)
    response = flask.make_response(jsonify(parametrized_component_view(cls_obj)))
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response


if __name__ == '__main__':
    app.run(debug=True)
