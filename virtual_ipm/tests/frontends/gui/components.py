# -*- coding: utf-8 -*-

#    Virtual-IPM is a software for simulating IPMs and other related devices.
#    Copyright (C) 2017  The IPMSim collaboration <http://ipmsim.gitlab.io/IPMSim>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import absolute_import, print_function, unicode_literals

import sys

from PyQt4 import QtGui

from virtual_ipm.configuration.frontends.qt.parameters import InvalidInputError
from virtual_ipm.frontends.gui.views import Beams, Device, ParticleGeneration, ParticleTracking, \
    ElectricGuidingField, MagneticGuidingField, Simulation, OutputRecorder


class TestWidget(QtGui.QWidget):
    def __init__(self, widget, parent=None):
        super(TestWidget, self).__init__(parent)
        self._widget = widget
        self._json_button = QtGui.QPushButton('JSON')
        self._xml_button = QtGui.QPushButton('XML')

        def print_json():
            try:
                print(self._widget.dump_as_json())
            except InvalidInputError as err:
                QtGui.QMessageBox.critical(self, err.__class__.__name__, str(err))

        def print_xml():
            try:
                print(self._widget.dump_as_xml())
            except InvalidInputError as err:
                QtGui.QMessageBox.critical(self, err.__class__.__name__, str(err))

        self._json_button.clicked.connect(print_json)
        self._xml_button.clicked.connect(print_xml)

        v_layout = QtGui.QVBoxLayout()
        v_layout.addWidget(widget)
        h_layout = QtGui.QHBoxLayout()
        h_layout.addStretch(1)
        h_layout.addWidget(self._json_button)
        h_layout.addWidget(self._xml_button)
        v_layout.addLayout(h_layout)
        self.setLayout(v_layout)
        self.setWindowTitle(self._widget.__class__.__name__)


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)

    test_widgets = [
        TestWidget(Beams()),
        # TestWidget(Device()),
        # TestWidget(ParticleGeneration()),
        # TestWidget(ParticleTracking()),
        # TestWidget(ElectricGuidingField()),
        # TestWidget(MagneticGuidingField()),
        # TestWidget(Simulation()),
        # TestWidget(OutputRecorder()),
    ]
    for test_widget in test_widgets:
        # print(
        #     test_widget.windowTitle(), ': ',
        #     test_widget._widget._interface.CONFIG_PATH, ', ',
        #     test_widget._widget._interface.CONFIG_PATH_TO_IMPLEMENTATION
        # )
        # for par in test_widget._widget._interface.get_parameters():
        #     print(par)
        test_widget.show()

    sys.exit(app.exec_())
