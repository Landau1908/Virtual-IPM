# -*- coding: utf-8 -*-

#    Virtual-IPM is a software for simulating IPMs and other related devices.
#    Copyright (C) 2017  The IPMSim collaboration <http://ipmsim.gitlab.io/IPMSim>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import six
import sys
import xml.etree.ElementTree as XMLET
from PyQt4 import QtGui

from virtual_ipm.configuration.frontends.qt.parameters import BoolInput, IntegerInput, NumberInput, StringInput, VectorInput, \
    DupletInput, TripletInput
from virtual_ipm.configuration.frontends.qt.parameters import InvalidInputError


class TestArea(QtGui.QWidget):
    def __init__(self, parent=None):
        super(TestArea, self).__init__(parent)

        layout = QtGui.QVBoxLayout()

        dump_as_xml_button = QtGui.QPushButton('Dump as XML')
        dump_as_xml_button.clicked.connect(self.dump_as_xml)

        validate_input_button = QtGui.QPushButton('Validate input')
        validate_input_button.clicked.connect(self.validate_input)

        h_layout = QtGui.QHBoxLayout()
        h_layout.addWidget(dump_as_xml_button)
        h_layout.addWidget(validate_input_button)
        h_layout.addStretch(1)
        layout.addLayout(h_layout)

        self.inputs = []

        def add_input(cls):
            h_layout = QtGui.QHBoxLayout()
            h_layout.addWidget(QtGui.QLabel(cls.__name__))
            self.inputs.append(cls(cls.PEER(cls.PEER.__name__, unit='1')))
            h_layout.addWidget(self.inputs[-1])
            h_layout.addStretch(1)
            layout.addLayout(h_layout)

        for input_form in [BoolInput, IntegerInput, NumberInput, StringInput, VectorInput, DupletInput, TripletInput]:
            add_input(input_form)

        self.setLayout(layout)

    def dump_as_xml(self):
        QtGui.QMessageBox.info(self, 'Not available', 'XML dumping test is not implemented yet')

    def validate_input(self):
        for input in self.inputs:
            element = XMLET.Element('Test')
            try:
                input.dump_on_xml_element(element)
            except InvalidInputError as err:
                QtGui.QMessageBox.critical(self, err.__class__.__name__, six.text_type(err))
                break


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)

    main_widget = TestArea()
    main_widget.show()

    sys.exit(app.exec_())

