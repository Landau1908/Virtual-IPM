# -*- coding: utf-8 -*-

#    Virtual-IPM is a software for simulating IPMs and other related devices.
#    Copyright (C) 2017  The IPMSim collaboration <http://ipmsim.gitlab.io/IPMSim>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import six
import sys
import xml.etree.ElementTree as XMLET
from PyQt4 import QtGui

import virtual_ipm.tests.simulation.configuration.parameters as parameters
from virtual_ipm.configuration.frontends.qt.parameters import InvalidInputError
from virtual_ipm.frontends.gui.views import ParameterInput


def new_dump_xml_tester(view):
    def test_dump_xml():
        root = XMLET.Element('Test')
        for parameter in view.input_fields:
            try:
                parameter.dump_on_xml_element(root)
            except InvalidInputError as err:
                QtGui.QMessageBox.critical(view, 'Invalid input', six.text_type(err))
    return test_dump_xml


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)

    component_cls = getattr(parameters, sys.argv[1])

    v_layout = QtGui.QVBoxLayout()

    parameter_view = ParameterInput(component_cls.CONFIG_PARAMETERS)
    v_layout.addWidget(parameter_view)

    button = QtGui.QPushButton('dump')
    button.clicked.connect(new_dump_xml_tester(parameter_view))
    v_layout.addWidget(button)

    main_widget = QtGui.QWidget()
    main_widget.setLayout(v_layout)
    main_widget.show()

    sys.exit(app.exec_())
