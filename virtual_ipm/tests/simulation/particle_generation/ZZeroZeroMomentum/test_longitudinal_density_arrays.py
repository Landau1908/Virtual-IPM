# -*- coding: utf-8 -*-

#    Virtual-IPM is a software for simulating IPMs and other related devices.
#    Copyright (C) 2017  The IPMSim collaboration <http://ipmsim.gitlab.io/IPMSim>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import absolute_import, unicode_literals

import sys

from anna import XMLAdaptor
import injector
import matplotlib.pyplot as plt
import numpy

from virtual_ipm.di.bindings import create_bindings
import virtual_ipm.di.models as models
import virtual_ipm.log as log


assert len(sys.argv) >= 2, 'Not enough arguments'

log.to_console()

config = XMLAdaptor(sys.argv[1])
injector_container = injector.Injector(create_bindings(config))
model = injector_container.get(models.particle_generation)

model.prepare()

plt.plot(
    numpy.arange(model._longitudinal_density_arrays[0].size),
    model._longitudinal_density_arrays[0]
)
plt.show()
