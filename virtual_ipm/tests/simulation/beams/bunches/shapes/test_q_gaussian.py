from argparse import ArgumentParser
import matplotlib.pyplot as plt
import numpy as np
from virtual_ipm.simulation.beams.bunches.shapes import QGaussian


parser = ArgumentParser()
parser.add_argument('q', nargs=3, type=float)
parser.add_argument('b', nargs=3, type=float)
parser.add_argument('scale', nargs=3, type=float)

args = parser.parse_args()

QGaussian._q = args.q
QGaussian._beta = args.b
QGaussian._scale = args.scale

shape = QGaussian(None)

assert shape.length == 2. * args.scale[2], 'Length does not match'

print('x-max: ', shape.eval_x(0))
print('y-max: ', shape.eval_y(0))
print('z-max: ', shape.eval_z(0))

positions = shape.generate_positions_in_transverse_plane(1000, 0)
print('shape: ', positions.shape)
xs = positions[0, :]
ys = positions[1, :]

x_bins = np.linspace(-args.scale[0], args.scale[0], 100)
x_ref_prob = shape.normalized_density_at(np.stack([x_bins[:-1], np.zeros_like(x_bins[:-1]), np.zeros_like(x_bins[:-1])]))
x_hist = np.histogram(xs, bins=x_bins)
x_prob = x_hist[0].astype(float) / np.sum(x_hist[0]) * np.sum(x_ref_prob)

y_bins = np.linspace(-args.scale[1], args.scale[1], 100)
y_ref_prob = shape.normalized_density_at(np.stack([np.zeros_like(y_bins[:-1]), y_bins[:-1], np.zeros_like(y_bins[:-1])]))
y_hist = np.histogram(ys, bins=y_bins)
y_prob = y_hist[0].astype(float) / np.sum(y_hist[0]) * np.sum(y_ref_prob)

z_pos = np.linspace(-args.scale[2], args.scale[2], 100)
z_dens = shape.normalized_linear_density_at(z_pos)

plt.figure()
plt.title('x')
plt.plot(x_bins[:-1], x_prob, '-o', label='dist')
plt.plot(x_bins[:-1], x_ref_prob, '--x', label='ref')
plt.legend()

plt.figure()
plt.title('y')
plt.plot(y_bins[:-1], y_prob, '-o', label='dist')
plt.plot(y_bins[:-1], y_ref_prob, '--x', label='ref')
plt.legend()

plt.figure()
plt.title('z')
plt.plot(z_pos, z_dens, '-o', label='density')
plt.legend()

plt.show()
