import argparse

import anna
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy
from virtual_ipm.simulation.beams.bunches.shapes import HollowDCBeam


parser = argparse.ArgumentParser()
parser.add_argument('mode')


inner_radius = 5e-3   # [mm]
outer_radius = 10e-3  # [mm]
configuration = anna.JSONAdaptor(root={
    'Parameters/Energy': {
        'text': '1',
        'meta': {'unit': 'GeV'}
    },
    'Parameters/ParticleType/ChargeNumber': '1',
    'Parameters/ParticleType/RestEnergy': {
        'text': '%(proton mass energy equivalent in MeV)',
        'meta': {'unit': 'MeV'}
    },
    'BunchShape/Parameters/InnerRadius': {
        'text': '{0}'.format(inner_radius),
        'meta': {'unit': 'm'}
    },
    'BunchShape/Parameters/OuterRadius': {
        'text': '{0}'.format(outer_radius),
        'meta': {'unit': 'm'}
    },
    'BunchShape/Parameters/BeamCurrent': {
        'text': '1',
        'meta': {'unit': 'A'}
    }
})


def test_normalized_density_at():
    bunch_shape = HollowDCBeam(configuration)

    n_points = 100
    xs = numpy.linspace(-1.2 * outer_radius, 1.2 * outer_radius, n_points)
    ys = numpy.linspace(-1.2 * outer_radius, 1.2 * outer_radius, n_points)

    ys_grid, xs_grid = numpy.meshgrid(ys, xs)
    positions = numpy.stack((
        xs_grid.flatten(),
        ys_grid.flatten(),
        numpy.zeros(xs_grid.size)
    ))

    density_grid = bunch_shape.normalized_density_at(positions).reshape(xs_grid.shape)

    figure = plt.figure()
    # axes = figure.gca(projection='3d')
    # surface_plot = axes.plot_surface(
    #     xs_grid * 1.0e3, ys_grid * 1.0e3, density_grid,
    #     cmap=cm.coolwarm, linewidth=0, antialiased=False
    # )
    axes = figure.add_subplot(111)
    surface_plot = axes.pcolor(
        xs_grid * 1.0e3, ys_grid * 1.0e3, density_grid
    )
    axes.set_title('Normalized particle density')
    axes.set_xlabel('x [mm]')
    axes.set_ylabel('y [mm]')
    axes.grid()
    figure.colorbar(surface_plot, shrink=0.5, aspect=5)

    plt.show()


def test_generate_positions_in_transverse_plane():
    bunch_shape = HollowDCBeam(configuration)
    positions = bunch_shape.generate_positions_in_transverse_plane(10000, 0) * 1e3  # [m] -> [mm]

    figure = plt.figure()
    axes = figure.add_subplot(111)
    axes.scatter(
        positions[0],
        positions[1],
        s=1
    )
    axes.set_title('Transverse positions')
    axes.set_xlabel('x [mm]')
    axes.set_ylabel('y [mm]')
    axes.grid()

    plt.show()


if __name__ == '__main__':
    args = parser.parse_args()
    locals()['test_{0}'.format(args.mode)]()
