import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
from scipy.stats import gennorm, norm
from virtual_ipm.simulation.beams.bunches.shapes import GeneralizedGaussian


GeneralizedGaussian._transverse_shape = (2, 2)
GeneralizedGaussian._transverse_scale = (np.sqrt(2), np.sqrt(8))
GeneralizedGaussian._longitudinal_shape = 2
GeneralizedGaussian._longitudinal_scale = np.sqrt(2 * 1.5**2)

shape = GeneralizedGaussian(None)

print('Shape: ', shape._shape)
print('Scale: ', shape._scale)
print('Length: ', shape.length)

# Test long. density.

z_positions = np.linspace(-5, 5, 1000)
long_density = shape.normalized_linear_density_at(z_positions)

plt.figure()
plt.plot(z_positions, long_density, label='Gen.Gauss.')
plt.plot(z_positions, norm.pdf(z_positions, scale=1.5), '-.', label='Gaussian')
plt.title('Long. density')
plt.legend()

# Test transverse density.

x_positions = np.linspace(-5, 5, 1000)
y_positions = np.linspace(-10, 10, 1000)
d_along_x = shape.normalized_density_at(np.stack([x_positions,
                                                  np.zeros_like(x_positions),
                                                  np.zeros_like(x_positions)]))
d_along_y = shape.normalized_density_at(np.stack([np.zeros_like(y_positions),
                                                  y_positions,
                                                  np.zeros_like(y_positions)]))

plt.figure()
plt.plot(x_positions, d_along_x, label='Gen.Gauss. ~ x')
plt.plot(y_positions, d_along_y, label='Gen.Gauss. ~ y')
plt.plot(x_positions, norm.pdf(x_positions) / (2 * np.pi * 2 * 1.5), '-.', label='Gaussian ~ x')
plt.plot(y_positions, norm.pdf(y_positions, scale=2) / (2 * np.pi * 1.5), '-.', label='Gaussian ~ y')
plt.title('Transverse density')
plt.legend()

# Test transverse position generation.

pos = shape.generate_positions_in_transverse_plane(100000, 0)
x_hist = np.histogram(pos[0, :], bins=np.linspace(-10, 10, 500))
y_hist = np.histogram(pos[1, :], bins=np.linspace(-10, 10, 500))

gaussian = lambda x, *p: p[0] * norm.pdf(x, scale=p[1])

x_fit, x_cov = curve_fit(gaussian, x_hist[1][:-1], x_hist[0], p0=(x_hist[0].max(), 1.))
y_fit, y_cov = curve_fit(gaussian, y_hist[1][:-1], y_hist[0], p0=(y_hist[0].max(), 1.))

print('x-fit: ', x_fit)
print('x-cov: ', x_cov)
print('y-fit: ', y_fit)
print('y-cov: ', y_cov)

plt.figure()
plt.plot(x_hist[1][:-1], x_hist[0], label='Gen.Gauss. ~ x')
plt.plot(y_hist[1][:-1], y_hist[0], label='Gen.Gauss. ~ y')
plt.plot(x_hist[1][:-1], gaussian(x_hist[1][:-1], *x_fit), '-.', label='Gaussian ~ x')
plt.plot(y_hist[1][:-1], gaussian(y_hist[1][:-1], *y_fit), '-.', label='Gaussian ~ y')
plt.title('Transverse position generation')
plt.legend()

plt.show()
