# -*- coding: utf-8 -*-

#    Virtual-IPM is a software for simulating IPMs and other related devices.
#    Copyright (C) 2017  The IPMSim collaboration <http://ipmsim.gitlab.io/IPMSim>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest

from anna import XMLAdaptor
import injector
import numpy

import virtual_ipm.di as di
import virtual_ipm.di.bindings as bindings
import virtual_ipm.simulation.beams.bunches.electric_field_models as electric_field_models
import virtual_ipm.simulation.beams.bunches.shapes as shapes
import virtual_ipm.simulation.devices as devices


CONFIG_FILENAME = 'config.xml'
BUNCH_SHAPE_CLS = shapes.Gaussian
MODEL_CLS = electric_field_models.BassettiErskine

bunch_shape_cls_binding_key = injector.BindingKey('BunchShape')
model_class_binding_key = injector.BindingKey('ModelClass')


class Adaptor(object):
    @injector.inject(model_class=model_class_binding_key, bunch_shape_cls=bunch_shape_cls_binding_key,
                     device=di.components.device, configuration=di.components.configuration)
    def __init__(self, model_class, bunch_shape_cls, device, configuration):
        bunch_shape = bunch_shape_cls(configuration.get_configuration('Beams/Beam/BunchShape'))
        self.model = model_class(bunch_shape, device,
                                 configuration.get_configuration('Beams/Beam/BunchElectricFieldModel'))


class TestCase(unittest.TestCase):
    def setUp(self):
        configuration = XMLAdaptor(filepath=CONFIG_FILENAME)

        binding_modules = bindings.create_for_configuration(configuration) + \
            [
                bindings.create_binding('ParticleSupervisorModule', di.components.particle_supervisor, None,
                                        provider=injector.InstanceProvider),
                bindings.create_binding_for_component(devices.DeviceManager, di.components.device),
                bindings.create_binding_for_mutable(devices, di.models.device, configuration),
                bindings.create_binding('BunchShapeModule', bunch_shape_cls_binding_key, BUNCH_SHAPE_CLS,
                                        provider=injector.InstanceProvider),
                bindings.create_binding('ModelClassModule', model_class_binding_key, MODEL_CLS,
                                        provider=injector.InstanceProvider)
            ]
        self._model = injector.Injector(binding_modules).get(Adaptor).model

    def test_has_prepare(self):
        self.assertTrue(hasattr(self._model, 'prepare'))
        self.assertTrue(callable(self._model.prepare))

    def test_eval(self):
        self._model.prepare()
        position = numpy.zeros(shape=(3,), dtype=float)
        result = self._model.eval(position)
        self.assertIsInstance(result, numpy.ndarray)
        self.assertTrue(result.shape == (3,))

    def test_call_dispatches_to_eval(self):
        self._model.prepare()
        position = numpy.zeros(shape=(3,), dtype=float)
        result_eval = self._model.eval(position)
        result_call = self._model(position)
        self.assertEqual(tuple(result_eval), tuple(result_call))

    def do_nothing(self):
        pass


if __name__ == '__main__':
    unittest.main()
