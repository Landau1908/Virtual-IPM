# -*- coding: utf-8 -*-

#    Virtual-IPM is a software for simulating IPMs and other related devices.
#    Copyright (C) 2017  The IPMSim collaboration <http://ipmsim.gitlab.io/IPMSim>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys

import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import numpy

assert len(sys.argv) >= 7, 'Not enough arguments'

xs_grid = numpy.loadtxt(sys.argv[1])
ys_grid = numpy.loadtxt(sys.argv[2])
Ex = numpy.loadtxt(sys.argv[3])

xs_grid_ref = numpy.loadtxt(sys.argv[4])
ys_grid_ref = numpy.loadtxt(sys.argv[5])
Ex_ref = numpy.loadtxt(sys.argv[6])

grid_threshold = 1.0e-3

if numpy.any(numpy.abs(xs_grid / xs_grid_ref - 1.0) > grid_threshold):
    print >> sys.stderr, 'x-grid is different'

if numpy.any(numpy.abs(ys_grid / ys_grid_ref - 1.0) > grid_threshold):
    print >> sys.stderr, 'y-grid is different'


figure = plt.figure()
axes = figure.gca(projection='3d')
surface_plot = axes.plot_surface(xs_grid, ys_grid, Ex,
                                 cmap=cm.coolwarm, linewidth=0, antialiased=False)
axes.set_title('Ex in [V/m] (for a bunch population of 1)')
axes.set_xlabel('x [mm]')
axes.set_ylabel('y [mm]')
figure.colorbar(surface_plot, shrink=0.5, aspect=5)


figure = plt.figure()
axes = figure.gca(projection='3d')
surface_plot = axes.plot_surface(xs_grid, ys_grid, Ex_ref,
                                 cmap=cm.coolwarm, linewidth=0, antialiased=False)
axes.set_title('Ex reference in [V/m] (for a bunch population of 1)')
axes.set_xlabel('x [mm]')
axes.set_ylabel('y [mm]')
figure.colorbar(surface_plot, shrink=0.5, aspect=5)


Ex_divided_by_ref = Ex / Ex_ref

print 'Minimum Ex/Ex_ref: ', numpy.min(Ex_divided_by_ref), \
      '; Maximum Ex/Ex_ref: ', numpy.max(Ex_divided_by_ref)

figure = plt.figure()
axes = figure.gca(projection='3d')
surface_plot = axes.plot_surface(xs_grid, ys_grid, Ex_divided_by_ref,
                                 cmap=cm.coolwarm, linewidth=0, antialiased=False)
axes.set_title('Ex divided by Ex_ref')
axes.set_xlabel('x [mm]')
axes.set_ylabel('y [mm]')
figure.colorbar(surface_plot, shrink=0.5, aspect=5)


Ex_relative_deviation = (Ex - Ex_ref) / numpy.absolute(Ex_ref) * 100

print 'Minimum relative deviation: ', numpy.min(Ex_relative_deviation), \
      '; Maximum relative deviation', numpy.max(Ex_relative_deviation)

figure = plt.figure()
axes = figure.gca(projection='3d')
surface_plot = axes.plot_surface(xs_grid, ys_grid, Ex_relative_deviation,
                                 cmap=cm.coolwarm, linewidth=0, antialiased=False)
axes.set_title('Relative deviation of Ex in %')
axes.set_xlabel('x [mm]')
axes.set_ylabel('y [mm]')
figure.colorbar(surface_plot, shrink=0.5, aspect=5)


Ex_absolute_deviation = Ex - Ex_ref

print 'Minimum absolute deviation: ', numpy.min(Ex_absolute_deviation), \
      '; Maximum absolute deviation', numpy.max(Ex_absolute_deviation)

figure = plt.figure()
axes = figure.gca(projection='3d')
surface_plot = axes.plot_surface(xs_grid, ys_grid, Ex_absolute_deviation,
                                 cmap=cm.coolwarm, linewidth=0, antialiased=False)
axes.set_title('Absolute deviation of Ex in [V/m] (for a bunch population of 1)')
axes.set_xlabel('x [mm]')
axes.set_ylabel('y [mm]')
figure.colorbar(surface_plot, shrink=0.5, aspect=5)


plt.show()
