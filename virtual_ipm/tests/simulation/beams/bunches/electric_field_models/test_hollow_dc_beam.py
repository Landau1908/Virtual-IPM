import argparse

import anna
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy
import scipy.constants as constants
from virtual_ipm.simulation.beams.bunches.shapes import HollowDCBeam
from virtual_ipm.simulation.beams.bunches.electric_field_models import HollowDCBeam as \
    EFHollowDCBeam


parser = argparse.ArgumentParser()
parser.add_argument('dim')


inner_radius = 5e-3   # [mm]
outer_radius = 10e-3  # [mm]
configuration = anna.JSONAdaptor(root={
    'Parameters/Energy': {
        'text': '1',
        'meta': {'unit': 'GeV'}
    },
    'Parameters/ParticleType/ChargeNumber': '1',
    'Parameters/ParticleType/RestEnergy': {
        'text': '%(proton mass energy equivalent in MeV)',
        'meta': {'unit': 'MeV'}
    },
    'BunchShape/Parameters/InnerRadius': {
        'text': '{0}'.format(inner_radius),
        'meta': {'unit': 'm'}
    },
    'BunchShape/Parameters/OuterRadius': {
        'text': '{0}'.format(outer_radius),
        'meta': {'unit': 'm'}
    },
    'BunchShape/Parameters/BeamCurrent': {
        'text': '1',
        'meta': {'unit': 'A'}
    }
})

bunch_shape = HollowDCBeam(configuration)
field_model = EFHollowDCBeam(bunch_shape, None)


def setup_figure_and_axes(figure, axes, xlabel, ylabel, title):
    # axes.grid(True, lw=2)
    axes.grid(True, lw=2, which='major')
    axes.grid(True, lw=1, which='minor')
    plt.minorticks_on()
    axes.margins(0.05)
    axes.title.set_position((.5, 1.02))
    axes.tick_params(axis='both', which='major', pad=5)
    axes.set_xlabel(xlabel, fontsize=36)
    axes.set_ylabel(ylabel, fontsize=36)
    axes.tick_params(axis='x', labelsize=36)
    axes.tick_params(axis='y', labelsize=36)
    axes.set_title(title, fontsize=40)
    # axes.legend(shadow=False, fontsize=36)
    # figure.tight_layout()
    for spine in axes.spines.values():
        spine.set_linewidth(2)


def test_1d():
    scale = 5
    n_points = 1000
    positions_along_x = numpy.stack((
        numpy.linspace(-scale * outer_radius, scale * outer_radius, n_points),
        numpy.zeros(n_points),
        numpy.zeros(n_points)
    ))
    positions_along_y = numpy.stack((
        numpy.zeros(n_points),
        numpy.linspace(-scale * outer_radius, scale * outer_radius, n_points),
        numpy.zeros(n_points)
    ))

    field_along_x = field_model.eval(positions_along_x)
    field_along_y = field_model.eval(positions_along_y)

    figure = plt.figure()
    axes = figure.add_subplot(111)
    axes.plot(
        positions_along_x[0] * 1.0e3, field_along_x[0], label=r'$\rm E_x$', lw=3
    )
    axes.plot(
        positions_along_y[1] * 1.0e3, field_along_y[1], '--', label=r'$\rm E_y$', lw=3
    )
    setup_figure_and_axes(
        figure,
        axes,
        r'x, y [mm]',
        r'$\rm E_x$ [V/m]',
        r'1d-field-projections along $\rm y=0\; (E_x)\; and\; x=0\; (E_y)$'
    )

    plt.show()


def test_2d():
    scale = 5
    n_points = 1000
    x_positions = numpy.linspace(-scale * outer_radius, scale * outer_radius, n_points)
    y_positions = numpy.linspace(-scale * outer_radius, scale * outer_radius, n_points)

    y_grid, x_grid = numpy.meshgrid(y_positions, x_positions)

    positions = numpy.stack((
        x_grid.flatten(),
        y_grid.flatten(),
        numpy.zeros(x_grid.size)
    ))

    e_field_grid = field_model.eval(positions)[0].reshape(x_grid.shape) \
                   * constants.elementary_charge

    figure = plt.figure()
    axes = figure.add_subplot(111)
    surface_plot = axes.pcolor(
        x_grid * 1.0e3, y_grid * 1.0e3, e_field_grid
    )
    setup_figure_and_axes(
        figure,
        axes,
        r'x [mm]',
        r'y [mm]',
        r'Electric field in x-direction'
    )
    # axes.set_title('Electric field in x-direction', fontsize=36)
    # axes.set_xlabel('x [mm]', fontsize=36)
    # axes.set_ylabel('y [mm]', fontsize=36)
    # axes.grid()
    cbar = figure.colorbar(surface_plot, shrink=0.5, aspect=5)
    cbar.set_label(r'$\rm E_x\, [V/m]$', fontsize=28)
    cbar.ax.tick_params(axis='y', labelsize=28)

    plt.show()


if __name__ == '__main__':
    args = parser.parse_args()
    locals()['test_{}'.format(args.dim)]()
