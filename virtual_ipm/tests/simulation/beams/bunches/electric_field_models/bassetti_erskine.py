# -*- coding: utf-8 -*-

#    Virtual-IPM is a software for simulating IPMs and other related devices.
#    Copyright (C) 2017  The IPMSim collaboration <http://ipmsim.gitlab.io/IPMSim>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from argparse import Namespace

import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import numpy
import scipy.constants as constants

from virtual_ipm.simulation.beams.bunches.electric_field_models import BassettiErskine
from virtual_ipm.simulation.beams.bunches.shapes import Gaussian


# Sigma value for comparison with analytical solution for radially symmetric Gaussian bunch.
# Actually both methods are for different cases however at the boundaries of their valid regions they should coincide.
Gaussian._transverse_sigma = numpy.array((3.7e-3, 3.7e-3 - BassettiErskine.SIGMA_DIFFERENCE_THRESHOLD))
# Sigma values for PS case.
# Gaussian._transverse_sigma = numpy.array((3.7e-3, 1.4e-3))
Gaussian._longitudinal_sigma = 6.211691656301593
bunch_shape = Gaussian(None)

# For comparison with PoissonSOR2DGauss which uses a grid size of +/-3*sigma.
device = Namespace(x_min=-3 * 3.7e-3, x_max=3 * 3.7e-3, y_min=-3 * 1.4e-3, y_max=3 * 1.4e-3)

# Monkey patch.
BassettiErskine._grid_spacing_x = (device.x_max - device.x_min) / 100
BassettiErskine._grid_spacing_y = (device.y_max - device.y_min) / 100

model = BassettiErskine(bunch_shape, device, None)
model.prepare()

# ys_grid, xs_grid = numpy.meshgrid(model.yy[1: -1], model.xx[1: -1])
# zs_grid = numpy.zeros(shape=xs_grid.shape, dtype=float)
# grid_positions = numpy.dstack((xs_grid, ys_grid, zs_grid)).flatten().reshape((-1, 3)).T
#
# electric_field = model.eval(grid_positions)

sigma_z = bunch_shape.sigma[2]
sqrt_2pi = numpy.sqrt(2. * numpy.pi)

ys_grid, xs_grid = numpy.meshgrid(model.yy * 1000, model.xx * 1000)

figure = plt.figure()
axes = figure.gca(projection='3d')
surface_plot = axes.plot_surface(xs_grid, ys_grid, model.Ex * constants.elementary_charge / (sigma_z * sqrt_2pi),
                                 cmap=cm.coolwarm, linewidth=0, antialiased=False)
axes.set_title('Electric field in x-direction')
axes.set_xlabel('x [mm]')
axes.set_ylabel('y [mm]')
figure.colorbar(surface_plot, shrink=0.5, aspect=5)

# Strip of extra points that were added to prevent out-of-bounds situations.
numpy.savetxt('bassetti_erskine_x.txt', xs_grid[1:-1, 1:-1])
numpy.savetxt('bassetti_erskine_y.txt', ys_grid[1:-1, 1:-1])
# numpy.savetxt('bassetti_erskine_Ex.txt', model.Ex[1:-1, 1:-1] * constants.elementary_charge)
# Need to divide by sigma_z * sqrt(2pi) for comparison with Poisson SOR.
numpy.savetxt('bassetti_erskine_Ex.txt', model.Ex[1:-1, 1:-1] * constants.elementary_charge / (sigma_z * sqrt_2pi))

plt.show()
