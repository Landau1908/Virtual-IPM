# -*- coding: utf-8 -*-

#    Virtual-IPM is a software for simulating IPMs and other related devices.
#    Copyright (C) 2017  The IPMSim collaboration <http://ipmsim.gitlab.io/IPMSim>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from argparse import Namespace

import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import numpy
import scipy.constants as constants

from virtual_ipm.simulation.beams.bunches.electric_field_models import PoissonSOR2DGauss
from virtual_ipm.simulation.beams.bunches.shapes import Gaussian


# Monkey patch.
PoissonSOR2DGauss._grid_size = (100, 100)
PoissonSOR2DGauss._convergence_limit = PoissonSOR2DGauss._convergence_limit.default
PoissonSOR2DGauss._relaxation_factor = PoissonSOR2DGauss._relaxation_factor.default

# For comparison with analytical solution for radially symmetric Gaussian bunch.
Gaussian._transverse_sigma = numpy.array((3.7e-3, 3.7e-3))
# Sigma values for PS case.
# Gaussian._transverse_sigma = numpy.array((3.7e-3, 1.4e-3))
Gaussian._longitudinal_sigma = 6.211691656301593
bunch_shape = Gaussian(None)

model = PoissonSOR2DGauss(bunch_shape, None, None)
model.prepare()

# ys_grid, xs_grid = numpy.meshgrid(model._ys[1: -1], model._xs[1: -1])
# zs_grid = numpy.zeros(shape=xs_grid.shape, dtype=float)
# grid_positions = numpy.dstack((xs_grid, ys_grid, zs_grid)).flatten().reshape((-1, 3)).T
#
# electric_field = model.eval(grid_positions)

ys_grid, xs_grid = numpy.meshgrid(model._ys * 1000, model._xs * 1000)

figure = plt.figure()
axes = figure.gca(projection='3d')
surface_plot = axes.plot_surface(xs_grid, ys_grid, model.Ex * constants.elementary_charge,
                                 cmap=cm.coolwarm, linewidth=0, antialiased=False)
axes.set_title('Electric field in x-direction')
axes.set_xlabel('x [mm]')
axes.set_ylabel('y [mm]')
figure.colorbar(surface_plot, shrink=0.5, aspect=5)

numpy.savetxt('poisson_sor_2d_gauss_x.txt', xs_grid)
numpy.savetxt('poisson_sor_2d_gauss_y.txt', ys_grid)
numpy.savetxt('poisson_sor_2d_gauss_Ex.txt', model.Ex * constants.elementary_charge)

plt.show()
