# -*- coding: utf-8 -*-

#    Virtual-IPM is a software for simulating IPMs and other related devices.
#    Copyright (C) 2017  The IPMSim collaboration <http://ipmsim.gitlab.io/IPMSim>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
from __future__ import absolute_import, print_function, unicode_literals

from anna import XMLAdaptor
import sys

import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import numpy

import virtual_ipm.simulation.beams.bunches.electric_field_models as electric_field_models
import virtual_ipm.simulation.beams.bunches.shapes as bunch_shapes


assert len(sys.argv) >= 4, 'Not enough arguments'

try:
    electric_field_model_cls = getattr(electric_field_models, sys.argv[1])
except AttributeError:
    raise ValueError('Unknown bunch electric field model: %s' % sys.argv[1])

try:
    bunch_shape_cls = getattr(bunch_shapes, sys.argv[2])
except AttributeError:
    raise ValueError('Unknown bunch shape: %s' % sys.argv[2])

config = XMLAdaptor(sys.argv[3])

bunch_shape = bunch_shape_cls(config)
electric_field_model = electric_field_model_cls(bunch_shape, None, config)

positions = numpy.loadtxt(config.get_text('PositionsFile'))

electric_field_vectors = electric_field_model.eval(positions)
electric_field_vectors *= 4e9  # 4 charges per particle, 1e9 particles.
electric_field_vectors /= 1000.  # [V/m] -> [V/mm]
positions *= 1000.  # [m] -> [mm]


figure = plt.figure()
axes = figure.add_subplot(111)
axes.plot(positions[1], electric_field_vectors[2])
# axes.plot(positions[2], electric_field_vectors[0], 'g-', label='Ex')
# axes.plot(positions[2], electric_field_vectors[1], 'r--', label='Ey')
axes.set_xlabel('y [mm]')
axes.set_ylabel('Ez [V/mm]')
axes.set_title('z-component of electrical field along y-axis from -3b to 3b '
               'with offset z = 30mm '
               '(a = 50mm, b = 10mm)')
# axes.set_title('x- and y-component of electrical field along z-axis from -2a to 2a with an offset '
#                'x = b/2, y = b/2 '
#                '(a = 50mm, b = 10mm)')
# plt.legend()
plt.grid()

plt.show()
