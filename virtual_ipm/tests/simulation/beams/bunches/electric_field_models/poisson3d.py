# -*- coding: utf-8 -*-

#    Virtual-IPM is a software for simulating IPMs and other related devices.
#    Copyright (C) 2017  The IPMSim collaboration <http://ipmsim.gitlab.io/IPMSim>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

from argparse import Namespace

import numpy

from virtual_ipm.simulation.beams.bunches.electric_field_models import Poisson3D
from virtual_ipm.simulation.beams.bunches.shapes import Gaussian
import virtual_ipm.log as log

log.to_console()

# LHC 6.5 TeV case.
# sigma = (229e-6, 257e-6, 649.1095292846628)
# bunch_population = 1.3e11

# SIS-18 400 MeV case (see presentation D.Vilsmeier for GSI LOBI seminar, 18.05.2016).
# sigma = (5.0e-3, 5.0e-3, 91.486127285965239)
sigma = (5.0e-3, 5.0e-3, 0.005)
bunch_population = 1.0e9

device = Namespace(
    x_min=-4*sigma[0], x_max=4*sigma[0],
    y_min=-4*sigma[1], y_max=4*sigma[1],
    z_min=-4*sigma[2], z_max=4*sigma[2]
)

Gaussian._transverse_sigma = tuple(sigma[:2])
Gaussian._longitudinal_sigma = sigma[2]
shape = Gaussian(None)

Poisson3D._grid_points = (40, 40, 400)
Poisson3D._x_range = None
Poisson3D._y_range = None
Poisson3D._z_range = None
Poisson3D._field_value_outside = Poisson3D._field_value_outside.default
Poisson3D._dirichlet_boundary_value = Poisson3D._dirichlet_boundary_value.default
Poisson3D._polynomial_type = Poisson3D._polynomial_type.default
Poisson3D._polynomial_degree = Poisson3D._polynomial_degree.default
# Poisson3D._grid_size = (25, 25, 400)
poisson_solver = Poisson3D(shape, device, None)
poisson_solver.prepare()
print('Preparation complete.')

xs = numpy.linspace(device.x_min*0.99, device.x_max*0.99, 100)
# ys = numpy.linspace(device.y_min, device.y_max, 100)
zs = numpy.linspace(device.z_min*0.99, device.z_max*0.99, 1000)
# ys_grid, xs_grid = numpy.meshgrid(ys, xs)
zs_grid, xs_grid = numpy.meshgrid(zs, xs)

# positions = numpy.r_[
#     '0,2',
#     xs_grid.flatten(),
#     ys_grid.flatten(),
#     numpy.zeros(shape=(len(xs) * len(ys),)),
# ]

positions = numpy.r_[
    '0,2',
    xs_grid.flatten(),
    numpy.zeros(shape=(len(xs) * len(zs),)),
    zs_grid.flatten(),
]

field_values = poisson_solver.eval(positions) * bunch_population

numpy.savetxt('poisson3d_xs_grid.txt.gz', xs_grid)
# numpy.savetxt('poisson3d_ys_grid.txt.gz', ys_grid)
numpy.savetxt('poisson3d_zs_grid.txt.gz', zs_grid)
numpy.savetxt('poisson3d_field_values.txt.gz', field_values)
