# -*- coding: utf-8 -*-

#    Virtual-IPM is a software for simulating IPMs and other related devices.
#    Copyright (C) 2017  The IPMSim collaboration <http://ipmsim.gitlab.io/IPMSim>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from argparse import Namespace
import sys

import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import numpy
import scipy.constants as constants

from virtual_ipm.simulation.beams.bunches.electric_field_models import \
    AnalyticalSolutionForRadiallySymmetricGaussianBunch
from virtual_ipm.simulation.beams.bunches.shapes import Gaussian

assert len(sys.argv) >= 3, 'Not enough arguments'


# Sigma values for PS case; can't process those.
# Gaussian._transverse_sigma = numpy.array((3.7e-3, 1.4e-3))
# Sigma value for comparison with analytical solution for radially symmetric Gaussian bunch.
# Actually both methods are for different cases however at the boundaries of their valid regions they should coincide.
# Gaussian._transverse_sigma = \
#     numpy.array((3.7e-3, 3.7e-3 - AnalyticalSolutionForRadiallySymmetricGaussianBunch.SIGMA_DIFFERENCE_THRESHOLD))
# Model requires similar sigma_x and sigma_y.
Gaussian._transverse_sigma = numpy.array((3.7e-3, 3.7e-3))
Gaussian._longitudinal_sigma = 6.211691656301593
bunch_shape = Gaussian(None)

model = AnalyticalSolutionForRadiallySymmetricGaussianBunch(bunch_shape, None, None)
model.prepare()

xs_grid = numpy.loadtxt(sys.argv[1]) * 1.0e-3
ys_grid = numpy.loadtxt(sys.argv[2]) * 1.0e-3
grid_shape = xs_grid.shape
positions = numpy.array((xs_grid.flatten(), ys_grid.flatten(), numpy.zeros(xs_grid.flatten().shape, dtype=float)))
electric_field = model.eval(positions)
Ex_grid = electric_field[0].reshape(grid_shape)
Ey_grid = electric_field[1].reshape(grid_shape)

figure = plt.figure()
axes = figure.gca(projection='3d')
surface_plot = axes.plot_surface(xs_grid, ys_grid, Ex_grid, cmap=cm.coolwarm, linewidth=0, antialiased=False)
axes.set_title('Electric field in x-direction')
axes.set_xlabel('x [mm]')
axes.set_ylabel('y [mm]')
figure.colorbar(surface_plot, shrink=0.5, aspect=5)

numpy.savetxt('analytical_solution_for_radially_symmetric_gaussian_bunch_Ex.txt', Ex_grid)

plt.show()
