# -*- coding: utf-8 -*-

#    Virtual-IPM is a software for simulating IPMs and other related devices.
#    Copyright (C) 2017  The IPMSim collaboration <http://ipmsim.gitlab.io/IPMSim>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy

N = 1000

# xs = numpy.zeros(N, dtype=float)
# ys = numpy.zeros(N, dtype=float)
# zs = numpy.linspace(-100e-3, 100e-3, N)

xs = numpy.zeros(N, dtype=float)
ys = numpy.linspace(-30e-3, 30e-3, N)
zs = numpy.zeros(N, dtype=float) + 30e-3

positions = numpy.stack((xs, ys, zs))
numpy.savetxt('along_z_axis.txt', positions)
