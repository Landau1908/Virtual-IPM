# -*- coding: utf-8 -*-

#    Virtual-IPM is a software for simulating IPMs and other related devices.
#    Copyright (C) 2017  The IPMSim collaboration <http://ipmsim.gitlab.io/IPMSim>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import sys

import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import numpy

assert len(sys.argv) >= 2, 'Not enough arguments'

prefix = sys.argv[1]
xs_grid = numpy.loadtxt(prefix + 'xs_grid.txt.gz') * 1000.
# ys_grid = numpy.loadtxt(prefix + 'ys_grid.txt.gz') * 1000.
zs_grid = numpy.loadtxt(prefix + 'zs_grid.txt.gz') * 1000.
field_values = numpy.loadtxt(prefix + 'field_values.txt.gz') / 1.0e6  # [V/m] -> [MV/m]
# field_values *= 6500./0.938  # Boost to lab frame (multiply by gamma).
# Ex_grid = field_values[0].reshape(xs_grid.shape)
Ez_grid = field_values[2].reshape(xs_grid.shape) * 1.0e6  # [MV/m] -> [V/m]

print('Ez_max: ', numpy.max(Ez_grid.flatten()))

figure = plt.figure()
axes = figure.gca(projection='3d')

# surface_plot = axes.plot_surface(
#     xs_grid, ys_grid, Ex_grid,
#     cmap=cm.coolwarm, linewidth=0, antialiased=False
# )
# axes.set_title('E_x in x-y-plane')
# axes.set_xlabel('x [mm]')
# axes.set_ylabel('y [mm]')
# axes.set_zlabel('Ex [MV/m] [lab frame]')

surface_plot = axes.plot_surface(
    xs_grid, zs_grid, Ez_grid,
    cmap=cm.coolwarm, linewidth=0, antialiased=False
)
axes.set_title('E_z in x-z-plane')
axes.set_xlabel('x [mm]')
axes.set_ylabel('z [mm]')
axes.set_zlabel('Ez [V/m] [lab frame]')

figure.colorbar(surface_plot, shrink=0.5, aspect=5)
plt.show()
