# -*- coding: utf-8 -*-

#    Virtual-IPM is a software for simulating IPMs and other related devices.
#    Copyright (C) 2017  The IPMSim collaboration <http://ipmsim.gitlab.io/IPMSim>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from numpy import sqrt, exp, pi, sign, abs
import scipy.constants as constants

from wfun import wfun

eps0 = constants.epsilon_0


def BassErsk(xin, yin, sigmax, sigmay):
    """
    Computes the bunch eletric field on a two-dimensional grid for an elliptical bunch (sigmax must not equal sigmay for this formula).
    The formula is derived by M. Bassetti & G.A. Erskine: 'Closed Expression for the Electrical Field of a Two-Dimensional Gaussian Charge', Geneva (CERN), 1980
    wfun will pass the required values on to errfffor which is a Fortran implementation of the complex error function. wfun acts as a wrapper for errfffor because the latter takes real and imaginary part separately instead of a complex number.

    input: xin (x-position), yin (y-position), sigmax (RMS of the Gaussian shape along x), sigmay (RMS of the Gaussian shape along y)
    output: Ex (bunch field in x-direction), Ey (bunch field in y-direction)
    """
        
    global eps0

    x = abs(xin)
    y = abs(yin)
    
    if sigmax > sigmay:
    
        S = sqrt(2.*(sigmax*sigmax - sigmay*sigmay))
        factBE = 1. / (2.*eps0*sqrt(pi)*S)
        etaBE = sigmay/sigmax*x + 1j*sigmax/sigmay*y  # use complex numbers to pass the values to the (complex) error function
        zetaBE = x + 1j*y
        
        val = factBE * (wfun(zetaBE/S) - exp( -x*x/(2.*sigmax*sigmax) - y*y/(2.*sigmay*sigmay) ) * wfun(etaBE/S) )
           
        Ex = abs(val.imag) * sign(xin)
        Ey = abs(val.real) * sign(yin)
    
    else:

        S = sqrt(2.*(sigmay*sigmay - sigmax*sigmax))
        factBE = 1. / (2.*eps0*sqrt(pi)*S)
        etaBE = sigmax/sigmay*y + 1j*sigmay/sigmax*x  # use complex numbers to pass the values to the (complex) error function
        yetaBE = y + 1j*x
        
        val = factBE * (wfun(yetaBE/S) - exp( -y*y/(2.*sigmay*sigmay) - x*x/(2.*sigmax*sigmax) ) * wfun(etaBE/S) )
           
        Ey = abs(val.imag) * sign(yin)
        Ex = abs(val.real) * sign(xin)
         
    return Ex, Ey