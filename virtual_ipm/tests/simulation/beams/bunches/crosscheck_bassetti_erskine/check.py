# -*- coding: utf-8 -*-

#    Virtual-IPM is a software for simulating IPMs and other related devices.
#    Copyright (C) 2017  The IPMSim collaboration <http://ipmsim.gitlab.io/IPMSim>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from argparse import Namespace
import itertools
import unittest

import numpy
import scipy.constants as constants
from scipy.constants import physical_constants

from virtual_ipm.simulation.beams.bunches import Bunch
from virtual_ipm.simulation.beams.bunches.electric_field_models import BassettiErskine

from BassErsk import BassErsk
import beam_longitudinal_profile as blp
from int_field_for import int_field as interpolate_field


class TestFieldEval(unittest.TestCase):
    sigma_xs = (869e-6, 346e-6, 229e-6)
    sigma_ys = (979e-6, 390e-6, 257e-6)

    x_limits = (-0.1, 0.1)
    y_limits = (-0.1, 0.1)
    number_of_points = 1000

    number_of_digits_tolerance = 12

    def setUp(self):
        print 'Test field evaluation'

    def test_zero_x(self):
        xs = numpy.zeros(shape=(1,), dtype=float)
        ys = numpy.linspace(self.y_limits[0], self.y_limits[1], self.number_of_points)
        self.run_cases(xs, ys)

    def test_zero_y(self):
        xs = numpy.linspace(self.x_limits[0], self.x_limits[1], self.number_of_points)
        ys = numpy.zeros(shape=(1,), dtype=float)
        self.run_cases(xs, ys)

    def test_nonzero_x_and_y(self):
        xs = numpy.linspace(self.x_limits[0], self.x_limits[1], self.number_of_points)
        ys = numpy.linspace(self.y_limits[0], self.y_limits[1], self.number_of_points)
        self.run_cases(xs, ys)

    def run_cases(self, xs, ys):
        for sigma_x, sigma_y in itertools.izip(self.sigma_xs, self.sigma_ys):
            for x in xs:
                for y in ys:
                    self.compare_results(x, y, sigma_x, sigma_y)

    def compare_results(self, x, y, sigma_x, sigma_y):
        result = BassettiErskine.compute_field_at(x, y, sigma_x, sigma_y)
        reference = BassErsk(x, y, sigma_x, sigma_y)

        result = list(result)
        reference = list(reference)

        # Normalize results so the number of decimal places equal the number of significant digits - 1.
        if result[0] != 0.:
            result[0] /= 10 ** (int(numpy.log10(abs(result[0]))))
        if result[1] != 0.:
            result[1] /= 10 ** (int(numpy.log10(abs(result[1]))))
        if reference[0] != 0.:
            reference[0] /= 10 ** (int(numpy.log10(abs(reference[0]))))
        if reference[1] != 0.:
            reference[1] /= 10 ** (int(numpy.log10(abs(reference[1]))))

        self.assertAlmostEqual(result[0], reference[0], places=self.number_of_digits_tolerance)
        self.assertAlmostEqual(result[1], reference[1], places=self.number_of_digits_tolerance)


class TestFieldInterpolation(unittest.TestCase):
    number_of_digits_tolerance = 12
    number_of_digits_tolerance_numpy_port = 4

    def setUp(self):

        print 'Test field interpolation'

        bunch_shape = Namespace(sigma=(800e-6, 900e-6, 300.))
        device = Namespace(x_min=-50e-3, x_max=50e-3, y_min=-50e-3, y_max=50e-3)
        configuration = None

        # Monkey patch the model.
        BassettiErskine._grid_spacing_x = 0.1e-3
        BassettiErskine._grid_spacing_y = 0.1e-3

        self._model = BassettiErskine(bunch_shape, device, configuration)
        self._model.prepare()

        nx = int((device.x_max - device.x_min) / BassettiErskine._grid_spacing_x)
        ny = int((device.y_max - device.y_min) / BassettiErskine._grid_spacing_y)

        self.xx = numpy.linspace(device.x_min, device.x_max, nx)
        self.yy = numpy.linspace(device.y_min, device.y_max, ny)

        self.x_min = device.x_min
        self.y_min = device.y_min

    def test_nonzero_x_and_y(self):
        print 'Testing original interpolation'

        xs = self.xx[:-2:5] + BassettiErskine._grid_spacing_x / 2.
        ys = self.yy[:-2:5] + BassettiErskine._grid_spacing_y / 2.

        for i, x in enumerate(xs):
            print 'Running x=%e (%d of %d)' % (x, i+1, len(xs))
            for y in ys:
                self.compare_results(x, y)

    def idle_test_numpy_port_nonzero_x_and_y(self):
        print 'Testing numpy port'

        xs = self.xx[:-2:5] + BassettiErskine._grid_spacing_x / 2.
        ys = self.yy[:-2:5] + BassettiErskine._grid_spacing_y / 2.

        for i, x in enumerate(xs):
            print 'Running x=%e (%d of %d)' % (x, i + 1, len(xs))
            for y in ys:
                self.compare_results_numpy_port(x, y)

    def compare_results(self, x, y):
        xs = numpy.array([x])
        ys = numpy.array([y])

        result = self._model.interpolate_field_at(xs, ys)
        result = result[0][0], result[1][0]

        reference = interpolate_field(xs, ys, self._model.x_min, self._model.y_min, BassettiErskine._grid_spacing_x,
                                      BassettiErskine._grid_spacing_y, self._model.Ex, self._model.Ey)
        reference = reference[0][0], reference[1][0]

        result = list(result)
        reference = list(reference)

        # Normalize results so the number of decimal places equal the number of significant digits - 1.
        if result[0] != 0.:
            result[0] /= 10 ** (int(numpy.log10(abs(result[0]))))
        if result[1] != 0.:
            result[1] /= 10 ** (int(numpy.log10(abs(result[1]))))
        if reference[0] != 0.:
            reference[0] /= 10 ** (int(numpy.log10(abs(reference[0]))))
        if reference[1] != 0.:
            reference[1] /= 10 ** (int(numpy.log10(abs(reference[1]))))

        self.assertAlmostEqual(result[0], reference[0], places=self.number_of_digits_tolerance)
        self.assertAlmostEqual(result[1], reference[1], places=self.number_of_digits_tolerance)

    def compare_results_numpy_port(self, x, y):
        result = self._model.interpolate_field_at_using_searchsorted(x, y)
        reference = self._model.interpolate_field_at_scalars_only(x, y)

        result = list(result)
        reference = list(reference)

        # Normalize results so the number of decimal places equal the number of significant digits - 1.
        if result[0] != 0.:
            result[0] /= 10 ** (int(numpy.log10(abs(result[0]))))
        if result[1] != 0.:
            result[1] /= 10 ** (int(numpy.log10(abs(result[1]))))
        if reference[0] != 0.:
            reference[0] /= 10 ** (int(numpy.log10(abs(reference[0]))))
        if reference[1] != 0.:
            reference[1] /= 10 ** (int(numpy.log10(abs(reference[1]))))

        self.assertAlmostEqual(result[0], reference[0], places=self.number_of_digits_tolerance_numpy_port)
        self.assertAlmostEqual(result[1], reference[1], places=self.number_of_digits_tolerance_numpy_port)


class TestFieldInLabFrame(unittest.TestCase):
    number_of_digits_tolerance = 9

    def setUp(self):

        print 'Test field in lab frame'

        sigma_z_bunch_frame = 300.
        bunch_shape = Namespace(sigma=(800e-6, 900e-6, sigma_z_bunch_frame), length=8*sigma_z_bunch_frame)
        device = Namespace(x_min=-50e-3, x_max=50e-3, y_min=-50e-3, y_max=50e-3)
        configuration = None

        # Monkey patch the model.
        BassettiErskine._grid_spacing_x = 1.0e-3
        BassettiErskine._grid_spacing_y = 1.0e-3

        model = BassettiErskine(bunch_shape, device, configuration)
        model.prepare()

        # Monkey patch the Bunch.
        Bunch._energy = 450e9
        Bunch._bunch_population = 1
        Bunch._particle_type = Namespace(rest_energy=physical_constants['proton mass energy equivalent in MeV'][0] * 1.0e6)

        self._bunch = Bunch(bunch_shape, model, 0, numpy.array([0, 0]), None, None)

        self.xx = model.xx[::10]
        self.yy = model.yy[::10]

    def work_in_progress_test_constant_time(self):
        sigma_z = self._bunch.bunch_shape.sigma[2]
        zs = numpy.linspace(-4 * sigma_z, 4 * sigma_z, 100)

    def test_constant_z(self):
        sigma_z_lab_frame = self._bunch.bunch_shape.sigma[2] / self._bunch.gamma
        ts = numpy.linspace(0., 5e-9, 3000)

        # PyECLOUD-BGI computations.
        z_offs = 4.0 * sigma_z_lab_frame
        lam_t_array = blp.bunch_train3(ts, sigma_z_lab_frame, z_offs, self._bunch.beta)
        lam_t_array = self._bunch.population * lam_t_array
        scaling_factors_pyecloud = lam_t_array * constants.elementary_charge

        ts *= constants.speed_of_light

        for x in self.xx:
            for y in self.yy:
                for t, sfp in zip(ts, scaling_factors_pyecloud):
                    self.compare_results(x, y, t, sfp)

    def compare_results(self, x, y, t, scaling_factor_pyecloud):
        result = self._bunch.electric_field_at(numpy.array([t, x, y, 0.]).reshape(4, 1))
        result = result[0][0], result[1][0]

        reference = self._bunch.bunch_electric_field_model.interpolate_field_at(x, y)
        reference = reference[0] * scaling_factor_pyecloud, reference[1] * scaling_factor_pyecloud

        result = list(result)
        reference = list(reference)

        # Normalize results so the number of decimal places equal the number of significant digits - 1.
        if result[0] != 0.:
            result[0] /= 10 ** (int(numpy.log10(abs(result[0]))))
        if result[1] != 0.:
            result[1] /= 10 ** (int(numpy.log10(abs(result[1]))))
        if reference[0] != 0.:
            reference[0] /= 10 ** (int(numpy.log10(abs(reference[0]))))
        if reference[1] != 0.:
            reference[1] /= 10 ** (int(numpy.log10(abs(reference[1]))))

        self.assertAlmostEqual(result[0], reference[0], places=self.number_of_digits_tolerance)
        self.assertAlmostEqual(result[1], reference[1], places=self.number_of_digits_tolerance)


if __name__ == '__main__':
    unittest.main()
