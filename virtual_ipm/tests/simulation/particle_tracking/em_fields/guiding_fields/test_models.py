# -*- coding: utf-8 -*-

#    Virtual-IPM is a software for simulating IPMs and other related devices.
#    Copyright (C) 2017  The IPMSim collaboration <http://ipmsim.gitlab.io/IPMSim>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest

import numpy

from virtual_ipm.configuration import XMLAdaptor
import virtual_ipm.simulation.particle_tracking.em_fields.guiding_fields.models.electric as electric_models


CONFIG_FILENAME = 'config.xml'
MODEL_CLS = electric_models.UniformElectricField


class TestCase(unittest.TestCase):
    def setUp(self):
        configuration = XMLAdaptor(filepath=CONFIG_FILENAME)
        self._model = MODEL_CLS(configuration)

    def test_eval(self):
        self._model.prepare()
        time = 0.
        position = numpy.zeros(shape=(3,), dtype=float)
        result = self._model.eval(time, position)
        self.assertIsInstance(result, numpy.ndarray)
        self.assertTrue(result.shape == (3,))


if __name__ == '__main__':
    unittest.main()
