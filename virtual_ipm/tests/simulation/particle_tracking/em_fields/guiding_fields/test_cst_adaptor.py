# -*- coding: utf-8 -*-

#    Virtual-IPM is a software for simulating IPMs and other related devices.
#    Copyright (C) 2017  The IPMSim collaboration <http://ipmsim.gitlab.io/IPMSim>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import numpy
import pandas

from virtual_ipm.simulation.particle_tracking.em_fields.guiding_fields.models.mixin import CSTMap3D


def Ex_distribution(x):
    return numpy.exp(-numpy.einsum('ij, ij -> j', x, x))


def Ey_distribution(x):
    return numpy.einsum('ij, ij -> j', x, x)


def Ez_distribution(x):
    return numpy.exp(-numpy.sqrt(numpy.einsum('ij, ij -> j', x, x)))

nx, ny, nz = 40, 40, 40
xs = numpy.linspace(-2., 2., nx)
ys = numpy.linspace(-2., 2., ny)
zs = numpy.linspace(-2., 2., nz)

xs_grid, ys_grid, zs_grid = numpy.meshgrid(xs, ys, zs, indexing='ij')

positions = numpy.r_[
    '0,2',
    xs_grid.flatten(),
    ys_grid.flatten(),
    zs_grid.flatten()
]

Ex = Ex_distribution(positions)
Ey = Ey_distribution(positions)
Ez = Ez_distribution(positions)
Ex_grid = Ex.reshape((nx, ny, nz))
Ey_grid = Ey.reshape((nx, ny, nz))
Ez_grid = Ez.reshape((nx, ny, nz))

csv_filename = 'test_cst_adaptor.csv'
data = numpy.r_[
    '0,2',
    xs_grid.flatten(),
    ys_grid.flatten(),
    zs_grid.flatten(),
    Ex,
    Ey,
    Ez
]
data = numpy.transpose(data)
data_frame = pandas.DataFrame(
    data,
    columns=['x [m]', 'y [m]', 'z [m]', 'Ex [V/m]', 'Ey [V/m]', 'Ez [V/m]']
)
data_frame.to_csv(csv_filename)

CSTMap3D._filename = csv_filename
cst_map_adaptor = CSTMap3D(None)
cst_map_adaptor.prepare()


# Add 0th (time) component -> shape (4, ...)
sample_positions = numpy.r_[
    '0,2',
    numpy.zeros(xs_grid[:-1, :-1, :-1].size),
    xs_grid[:-1, :-1, :-1].flatten() + 4./nx/2.,
    ys_grid[:-1, :-1, :-1].flatten() + 4./ny/2.,
    zs_grid[:-1, :-1, :-1].flatten() + 4./nz/2.
]
x_sampled_grid = sample_positions[1].reshape((nx-1, ny-1, nz-1))
y_sampled_grid = sample_positions[2].reshape((nx-1, ny-1, nz-1))
z_sampled_grid = sample_positions[3].reshape((nx-1, ny-1, nz-1))
E_sampled = cst_map_adaptor.eval(sample_positions, None)
Ex_sampled_grid = E_sampled[0].reshape((nx-1, ny-1, nz-1))
Ey_sampled_grid = E_sampled[1].reshape((nx-1, ny-1, nz-1))
Ez_sampled_grid = E_sampled[2].reshape((nx-1, ny-1, nz-1))


# ----- Ex -----

figure = plt.figure()
axes = figure.gca(projection='3d')
surface_plot = axes.plot_surface(
    ys_grid[nx/2], zs_grid[nx/2], Ex_grid[nx/2],
    cmap=cm.coolwarm, linewidth=0, antialiased=False
)
axes.set_title('Ex in y-z-plane at x=0')
axes.set_xlabel('y')
axes.set_ylabel('z')
axes.set_zlabel('Ex')
figure.colorbar(surface_plot, shrink=0.5, aspect=5)


figure = plt.figure()
axes = figure.gca(projection='3d')
surface_plot = axes.plot_surface(
    y_sampled_grid[nx/2], z_sampled_grid[nx/2], Ex_sampled_grid[nx/2],
    cmap=cm.coolwarm, linewidth=0, antialiased=False
)
axes.set_title('Sampled: Ex in y-z-plane at x=0')
axes.set_xlabel('y')
axes.set_ylabel('z')
axes.set_zlabel('Ex')
figure.colorbar(surface_plot, shrink=0.5, aspect=5)


# ----- Ey -----

figure = plt.figure()
axes = figure.gca(projection='3d')
surface_plot = axes.plot_surface(
    ys_grid[nx/2], zs_grid[nx/2], Ey_grid[nx/2],
    cmap=cm.coolwarm, linewidth=0, antialiased=False
)
axes.set_title('Ey in y-z-plane at x=0')
axes.set_xlabel('y')
axes.set_ylabel('z')
axes.set_zlabel('Ey')
figure.colorbar(surface_plot, shrink=0.5, aspect=5)


figure = plt.figure()
axes = figure.gca(projection='3d')
surface_plot = axes.plot_surface(
    y_sampled_grid[nx/2], z_sampled_grid[nx/2], Ey_sampled_grid[nx/2],
    cmap=cm.coolwarm, linewidth=0, antialiased=False
)
axes.set_title('Sampled: Ey in y-z-plane at x=0')
axes.set_xlabel('y')
axes.set_ylabel('z')
axes.set_zlabel('Ey')
figure.colorbar(surface_plot, shrink=0.5, aspect=5)


# ----- Ez -----

figure = plt.figure()
axes = figure.gca(projection='3d')
surface_plot = axes.plot_surface(
    ys_grid[nx/2], zs_grid[nx/2], Ez_grid[nx/2],
    cmap=cm.coolwarm, linewidth=0, antialiased=False
)
axes.set_title('Ez in y-z-plane at x=0')
axes.set_xlabel('y')
axes.set_ylabel('z')
axes.set_zlabel('Ez')
figure.colorbar(surface_plot, shrink=0.5, aspect=5)


figure = plt.figure()
axes = figure.gca(projection='3d')
surface_plot = axes.plot_surface(
    y_sampled_grid[nx/2], z_sampled_grid[nx/2], Ez_sampled_grid[nx/2],
    cmap=cm.coolwarm, linewidth=0, antialiased=False
)
axes.set_title('Sampled: Ez in y-z-plane at x=0')
axes.set_xlabel('y')
axes.set_ylabel('z')
axes.set_zlabel('Ez')
figure.colorbar(surface_plot, shrink=0.5, aspect=5)


plt.show()
