# -*- coding: utf-8 -*-

#    Virtual-IPM is a software for simulating IPMs and other related devices.
#    Copyright (C) 2017  The IPMSim collaboration <http://ipmsim.gitlab.io/IPMSim>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest

from virtual_ipm.configuration import datatypes
import virtual_ipm.log as log
from virtual_ipm.configuration import ActionParameter, Configurable, IntegerParameter, NumberParameter, \
    StringParameter, VectorParameter, ParameterGroup, parametrize
from virtual_ipm.configuration.adaptors import ConfigurationAdaptor as Adaptor
from virtual_ipm.configuration.adaptors import XMLAdaptor
from virtual_ipm.utils import convert_camel_case_to_snake_case


class DeclarationTest(unittest.TestCase):
    def setUp(self):
        self.component = type('Component', (Configurable,), {})

    def tearDown(self):
        self.component = None

    def test_regular_parameter_types(self):
        parametrize(IntegerParameter('IntegerParameter'),
                        NumberParameter('NumberParameter', 'some_unit'),
                        StringParameter('StringParameter'),
                        VectorParameter('VectorParameter', 'some_unit'))(self.component)

        self.assertTrue(hasattr(self.component, '_' + convert_camel_case_to_snake_case('IntegerParameter')))
        self.assertTrue(hasattr(self.component, '_' + convert_camel_case_to_snake_case('NumberParameter')))
        self.assertTrue(hasattr(self.component, '_' + convert_camel_case_to_snake_case('StringParameter')))
        self.assertTrue(hasattr(self.component, '_' + convert_camel_case_to_snake_case('VectorParameter')))

    def test_parameter_groups(self):
        parametrize(ParameterGroup(
            NumberParameter('Path/To/Primary', 'ns')
        ).add_option(
            NumberParameter('Path/To/FirstOption', 'ns', default=1.0), lambda v: 2 * v
        ).add_option(
            NumberParameter('Path/To/SecondOption', 'ns', default=2.0), lambda v: 3 * v
        ).add_option(
            IntegerParameter('Path/To/ThirdOption', default=3), lambda v: 4.0 * v
        ).add_option(
            StringParameter('Path/To/FourthOption', default='four'), lambda v: 4.0
        ))(self.component)

        self.assertTrue(hasattr(self.component, '_' + convert_camel_case_to_snake_case('Primary')))

    def test_dependencies_on_other_parameters(self):
        with self.assertRaises(ValueError):
            parametrize(IntegerParameter('IntegerParameter'),
                            ActionParameter(IntegerParameter('AnotherIntegerParameter'), lambda p: p,
                                            depends_on=(convert_camel_case_to_snake_case('IntegerParameter'),))
                            )(self.component)

        parametrize(ActionParameter(IntegerParameter('AnotherIntegerParameter'), lambda p: p,
                                        depends_on=('integer_parameter',)),
                        integer_parameter=IntegerParameter('IntegerParameter')
                        )(self.component)

        self.assertTrue(hasattr(self.component, 'integer_parameter'))
        self.assertTrue(hasattr(self.component, '_' + convert_camel_case_to_snake_case('AnotherIntegerParameter')))


class ConfigurationTest(unittest.TestCase):
    CONFIG_FILENAME = 'configuration_test.xml'

    def setUp(self):
        self.configuration = XMLAdaptor(filepath=self.CONFIG_FILENAME)

    @staticmethod
    def generate_component():
        class Component(Configurable):
            CONFIG_PATH = '.'

            def __init__(self, configuration):
                super(Component, self).__init__(configuration)

        return Component

    def test_invalid_path(self):
        component = parametrize(
            IntegerParameter('InvalidPath')
        )(self.generate_component())
        with self.assertRaises(Adaptor.InvalidPathError):
            component(self.configuration)

    def test_invalid_integer(self):
        component = parametrize(
            IntegerParameter('InvalidInteger')
        )(self.generate_component())
        with self.assertRaises(IntegerParameter.InvalidParameterError):
            component(self.configuration)

    def test_invalid_number(self):
        component = parametrize(
            NumberParameter('InvalidNumber', 'unit')
        )(self.generate_component())
        with self.assertRaises(NumberParameter.InvalidParameterError):
            component(self.configuration)

    def test_invalid_vector_format(self):
        component = parametrize(
            VectorParameter('InvalidVectorFormat', 'unit')
        )(self.generate_component())
        with self.assertRaises(VectorParameter.InvalidParameterError):
            component(self.configuration)

    def test_invalid_vector_element(self):
        component = parametrize(
            VectorParameter('InvalidVectorElement', 'unit')
        )(self.generate_component())
        with self.assertRaises(VectorParameter.InvalidParameterError):
            component(self.configuration)

    def test_missing_unit_for_number_parameter(self):
        component = parametrize(
            NumberParameter('MissingUnitNumber', 'unit')
        )(self.generate_component())
        with self.assertRaises(NumberParameter.MissingUnitError):
            component(self.configuration)

    def test_missing_unit_for_vector_parameter(self):
        component = parametrize(
            VectorParameter('MissingUnitVector', 'unit')
        )(self.generate_component())
        with self.assertRaises(VectorParameter.MissingUnitError):
            component(self.configuration)


class ConversionTest(unittest.TestCase):
    CONFIG_FILENAME = 'conversion_test.xml'
    DECIMAL_PRECISION = 9

    def setUp(self):
        self.configuration = XMLAdaptor(filepath=self.CONFIG_FILENAME)

    @staticmethod
    def generate_component():
        class Component(Configurable):
            CONFIG_PATH = '.'

            def __init__(self, configuration):
                super(Component, self).__init__(configuration)

        return Component

    def test_length(self):
        component = parametrize(
            from_meter=NumberParameter('NumberInMeter', 'm'),
            from_milli=NumberParameter('NumberInMillimeter', 'm'),
            from_micro=NumberParameter('NumberInMicrometer', 'm')
        )(self.generate_component())(self.configuration)

        converter = datatypes.convert_to(datatypes.number)
        in_meter = converter(self.configuration.get_text('NumberInMeter'))
        in_milli = converter(self.configuration.get_text('NumberInMillimeter'))
        in_micro = converter(self.configuration.get_text('NumberInMicrometer'))

        self.assertAlmostEqual(component.from_meter, in_meter,          places=self.DECIMAL_PRECISION)
        self.assertAlmostEqual(component.from_milli, 1.0e-3 * in_milli, places=self.DECIMAL_PRECISION)
        self.assertAlmostEqual(component.from_micro, 1.0e-6 * in_micro, places=self.DECIMAL_PRECISION)

    def test_time(self):
        component = parametrize(
            from_seconds=NumberParameter('NumberInSeconds', 's'),
            from_nano=NumberParameter('NumberInNanoseconds', 's'),
        )(self.generate_component())(self.configuration)

        converter = datatypes.convert_to(datatypes.number)
        in_seconds = converter(self.configuration.get_text('NumberInSeconds'))
        in_nano = converter(self.configuration.get_text('NumberInNanoseconds'))

        self.assertAlmostEqual(component.from_seconds, in_seconds,       places=self.DECIMAL_PRECISION)
        self.assertAlmostEqual(component.from_nano,    1.0e-9 * in_nano, places=self.DECIMAL_PRECISION)

    def test_energy(self):
        component = parametrize(
            from_eV=NumberParameter('NumberInEv', 'eV'),
            from_keV=NumberParameter('NumberInKeV', 'eV'),
            from_MeV=NumberParameter('NumberInMeV', 'eV'),
            from_GeV=NumberParameter('NumberInGeV', 'eV'),
            from_TeV=NumberParameter('NumberInTeV', 'eV')
        )(self.generate_component())(self.configuration)

        converter = datatypes.convert_to(datatypes.number)
        in_eV = converter(self.configuration.get_text('NumberInEv'))
        in_keV = converter(self.configuration.get_text('NumberInKeV'))
        in_MeV = converter(self.configuration.get_text('NumberInMeV'))
        in_GeV = converter(self.configuration.get_text('NumberInGeV'))
        in_TeV = converter(self.configuration.get_text('NumberInTeV'))

        self.assertAlmostEqual(component.from_eV, in_eV, places=self.DECIMAL_PRECISION)
        self.assertAlmostEqual(component.from_keV, 1.0e3 * in_keV,  places=self.DECIMAL_PRECISION)
        self.assertAlmostEqual(component.from_MeV, 1.0e6 * in_MeV,  places=self.DECIMAL_PRECISION)
        self.assertAlmostEqual(component.from_GeV, 1.0e9 * in_GeV,  places=self.DECIMAL_PRECISION)
        self.assertAlmostEqual(component.from_TeV, 1.0e12 * in_TeV, places=self.DECIMAL_PRECISION)


if __name__ == '__main__':
    log.to_console()
    unittest.main()
