from argparse import Namespace

import matplotlib.pyplot as plt
import numpy
import six
from virtual_ipm.simulation.devices.models import BIF
from virtual_ipm.simulation.simulation import Progress


class Particles:
    def __init__(self, count):
        self._count = count

    def __len__(self):
        return self._count

    def __getitem__(self, item):
        self._count -= numpy.sum(item)
        return Particles(numpy.sum(item))


lifetime = 1.0e-9
sim_time = lifetime * 10
time_delta = lifetime / 1000
n_steps = int(sim_time / time_delta)
BIF._lifetime = lifetime
BIF._x_boundaries = None
BIF._y_boundaries = None
bif = BIF(
    None,
    Namespace(
        time_delta=time_delta
    )
)
bif.decay_times = []
bif.n_decays = []


def detect_patch(self, particles, progress):
    self.decay_times.append(progress.time)
    self.n_decays.append(len(particles))

setattr(
    bif,
    bif.detect.__name__,
    six.create_bound_method(detect_patch, bif)
)

print('Starting BIF test')
n_particles = 1000000
particles = Particles(n_particles)
for step in range(n_steps):
    if len(particles):
        bif.scan_particles(particles, Progress(step, n_steps, time_delta))
    else:
        break

figure = plt.figure()
axes = figure.add_subplot(111)
axes.plot(
    numpy.array(bif.decay_times) / 1.0e-9,
    bif.n_decays,
    label='BIF'
)
ts_theory = numpy.arange(n_steps) * time_delta
ys_theory = numpy.exp(-ts_theory / lifetime)
ys_theory *= n_particles / numpy.sum(ys_theory)
axes.plot(
    ts_theory / 1.0e-9,
    ys_theory,
    label='Theory',
    lw=3
)


def setup_figure_and_axes(figure, axes, xlabel, ylabel, title):
    axes.grid(True, lw=2)
    axes.margins(0.05)
    axes.title.set_position((.5, 1.02))
    axes.tick_params(axis='both', which='major', pad=5)
    axes.set_xlabel(xlabel, fontsize=36)
    axes.set_ylabel(ylabel, fontsize=36)
    axes.tick_params(axis='x', labelsize=36)
    axes.tick_params(axis='y', labelsize=36)
    # axes.set_yticks([])
    axes.set_title(title, fontsize=40)
    # axes.legend(loc='upper right', shadow=False, fontsize=26)
    axes.legend(shadow=False, fontsize=32)
    # figure.tight_layout()
    for spine in axes.spines.values():
        spine.set_linewidth(2)

setup_figure_and_axes(
    figure,
    axes,
    'Elapsed time [ns]',
    'Number of emissions',
    r'Estimated number of emissions ($\rm \tau = 1\,ns$)'
)

plt.show()
