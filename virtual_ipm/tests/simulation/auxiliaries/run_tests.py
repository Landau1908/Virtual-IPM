# -*- coding: utf-8 -*-

#    Virtual-IPM is a software for simulating IPMs and other related devices.
#    Copyright (C) 2017  The IPMSim collaboration <http://ipmsim.gitlab.io/IPMSim>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import types
import unittest

from virtual_ipm.simulation.auxiliaries import Particle, ParticleSet


def isinstance_test(attribute, type_):
    def test(self):
        self.assertIsInstance(getattr(self._particle, attribute), type_)
    return test


def add_isinstance_test(cls, attribute, type_):
    setattr(cls, 'test_type_%s' % attribute, types.MethodType(isinstance_test(attribute, type_), None, cls))


def has_shape_test(attribute, shape):
    def test(self):
        self.assertTrue(getattr(self._particle, attribute).shape == shape)
    return test


def add_has_shape_test(cls, attribute, shape):
    setattr(cls, 'test_shape_%s' % attribute, types.MethodType(has_shape_test(attribute, shape), None, cls))


class TestParticle(unittest.TestCase):
    particle_type = {'charge': 1.0, 'mass': 1.0}

    def setUp(self):
        self._particle = Particle(self.particle_type)


add_isinstance_test(TestParticle, 'x', float)
add_isinstance_test(TestParticle, 'y', float)
add_isinstance_test(TestParticle, 'z', float)
add_isinstance_test(TestParticle, 'px', float)
add_isinstance_test(TestParticle, 'py', float)
add_isinstance_test(TestParticle, 'pz', float)
add_isinstance_test(TestParticle, 'previous_x', float)
add_isinstance_test(TestParticle, 'previous_y', float)
add_isinstance_test(TestParticle, 'previous_z', float)
add_isinstance_test(TestParticle, 'previous_px', float)
add_isinstance_test(TestParticle, 'previous_py', float)
add_isinstance_test(TestParticle, 'previous_pz', float)
add_isinstance_test(TestParticle, 'relativistic_gamma', float)

add_has_shape_test(TestParticle, 'position', (3,))
add_has_shape_test(TestParticle, 'momentum', (3,))
add_has_shape_test(TestParticle, 'previous_position', (3,))
add_has_shape_test(TestParticle, 'previous_momentum', (3,))
add_has_shape_test(TestParticle, 'position_four_vector', (4,))


class TestParticleSet(unittest.TestCase):
    particle_type = {'charge': 1.0, 'mass': 1.0}
    n_particles = 10

    def setUp(self):
        self._particle = ParticleSet(self.particle_type)
        for _ in xrange(self.n_particles):
            self._particle.add(Particle(self.particle_type))


add_has_shape_test(TestParticleSet, 'x', (TestParticleSet.n_particles,))
add_has_shape_test(TestParticleSet, 'y', (TestParticleSet.n_particles,))
add_has_shape_test(TestParticleSet, 'z', (TestParticleSet.n_particles,))
add_has_shape_test(TestParticleSet, 'px', (TestParticleSet.n_particles,))
add_has_shape_test(TestParticleSet, 'py', (TestParticleSet.n_particles,))
add_has_shape_test(TestParticleSet, 'pz', (TestParticleSet.n_particles,))
add_has_shape_test(TestParticleSet, 'previous_x', (TestParticleSet.n_particles,))
add_has_shape_test(TestParticleSet, 'previous_y', (TestParticleSet.n_particles,))
add_has_shape_test(TestParticleSet, 'previous_z', (TestParticleSet.n_particles,))
add_has_shape_test(TestParticleSet, 'previous_px', (TestParticleSet.n_particles,))
add_has_shape_test(TestParticleSet, 'previous_py', (TestParticleSet.n_particles,))
add_has_shape_test(TestParticleSet, 'previous_pz', (TestParticleSet.n_particles,))
add_has_shape_test(TestParticleSet, 'relativistic_gamma', (TestParticleSet.n_particles,))
add_has_shape_test(TestParticleSet, 'position', (3, TestParticleSet.n_particles))
add_has_shape_test(TestParticleSet, 'momentum', (3, TestParticleSet.n_particles))
add_has_shape_test(TestParticleSet, 'previous_position', (3, TestParticleSet.n_particles))
add_has_shape_test(TestParticleSet, 'previous_momentum', (3, TestParticleSet.n_particles))
add_has_shape_test(TestParticleSet, 'position_four_vector', (4, TestParticleSet.n_particles))


if __name__ == '__main__':
    unittest.main()
