# -*- coding: utf-8 -*-

#    Virtual-IPM is a software for simulating IPMs and other related devices.
#    Copyright (C) 2017  The IPMSim collaboration <http://ipmsim.gitlab.io/IPMSim>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys

import matplotlib.pyplot as plt
import numpy
import pandas
from scipy.constants import physical_constants

assert len(sys.argv) >= 2, 'Not enough arguments'

mass = physical_constants['electron mass'][0]
T = 10e-9
N = 3200
dt = T / N
df = pandas.DataFrame.from_csv(sys.argv[1])

initial_positions = numpy.stack((
    df['initial x'],
    df['initial y'],
    df['initial z']
))
initial_velocities = numpy.stack((
    df['initial px'],
    df['initial py'],
    df['initial pz']
)) / mass

final_positions = numpy.stack((
    df['final x'],
    df['final y'],
    df['final z']
))
final_velocities = numpy.stack((
    df['final px'],
    df['final py'],
    df['final pz']
)) / mass

tofs = (N - numpy.array(df['initial sim. step'])) * dt

position_deviation = (final_positions - (initial_positions + initial_velocities * tofs)) * 1.0e6
velocity_deviation = final_velocities - initial_velocities

figure = plt.figure()
axes = figure.add_subplot(111)
axes.plot(numpy.arange(len(position_deviation[0])), position_deviation[0], 'o', label='x-position', markersize=1)
axes.plot(numpy.arange(len(position_deviation[1])), position_deviation[1], 'o', label='y-position', markersize=1)
axes.plot(numpy.arange(len(position_deviation[2])), position_deviation[2], 'o', label='z-position', markersize=1)
axes.set_ylabel('dx [um]')
axes.legend()

figure = plt.figure()
axes = figure.add_subplot(111)
axes.plot(numpy.arange(len(velocity_deviation[0])), velocity_deviation[0], 'o', label='x-velocity', markersize=1)
axes.plot(numpy.arange(len(velocity_deviation[1])), velocity_deviation[1], 'o', label='y-velocity', markersize=1)
axes.plot(numpy.arange(len(velocity_deviation[2])), velocity_deviation[2], 'o', label='z-velocity', markersize=1)
axes.set_ylabel('d_vx [m/s]')
axes.legend()

plt.show()
