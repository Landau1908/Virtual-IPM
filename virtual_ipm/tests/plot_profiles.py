from __future__ import print_function

import argparse

import matplotlib.pyplot as plt
import numpy
import pandas


parser = argparse.ArgumentParser()
parser.add_argument('files', nargs='+')


def setup_figure_and_axes(figure, axes, xlabel, ylabel, title):
    axes.grid(True, lw=2)
    axes.margins(0.05)
    axes.title.set_position((.5, 1.02))
    axes.tick_params(axis='both', which='major', pad=5)
    axes.set_xlabel(xlabel, fontsize=36)
    axes.set_ylabel(ylabel, fontsize=36)
    axes.tick_params(axis='x', labelsize=36)
    axes.tick_params(axis='y', labelsize=36)
    axes.set_title(title, fontsize=40)
    axes.legend(shadow=False, fontsize=36)
    # figure.tight_layout()
    for spine in axes.spines.values():
        spine.set_linewidth(2)


def create_histogram(x_samples):
    x_samples *= 1.0e3  # [m] -> [mm]
    dx = 10e-3  # [mm]
    n_bins = int((numpy.max(x_samples) - numpy.min(x_samples)) / dx)
    bins, edges = numpy.histogram(x_samples, bins=n_bins)
    centers = edges[:-1] + (edges[1] - edges[0]) / 2.
    print('Number of bins: ', len(bins))
    print('Mean: ', numpy.mean(bins * centers))
    bins = bins.astype(float)
    bins *= 100000 / numpy.sum(bins)
    # return centers, bins
    return rebin_histogram(centers, bins, rebin=int(len(bins)/250))


def create_histogram_vipm(filepath, what):
    df = pandas.DataFrame.from_csv(filepath)
    return create_histogram(df['{} x'.format(what)])


def rebin_histogram(centers, bins, rebin=20):
    if len(bins) % rebin != 0:
        bins = bins[:-(len(bins) % rebin)]
        centers = centers[:-(len(centers) % rebin)]
    bins = bins.reshape((-1, rebin)).mean(1)
    centers = centers.reshape((-1, rebin)).mean(1)
    return centers, bins


if __name__ == '__main__':
    args = parser.parse_args()

    figure = plt.figure()
    axes = figure.add_subplot(111)

    for i, filename in enumerate(args.files):
        axes.plot(
            *create_histogram_vipm(filename, 'initial'),
            label='#{0} (initial)'.format(i),
            lw=2
        )
        axes.plot(
            *create_histogram_vipm(filename, 'final'),
            label='#{0} (final)'.format(i),
            lw=2
        )

    setup_figure_and_axes(
        figure,
        axes,
        'x-position [mm]',
        '[a.u.]',
        'Initial and final profiles'
    )

    plt.show()
