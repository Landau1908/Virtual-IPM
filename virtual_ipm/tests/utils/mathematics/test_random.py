from time import clock
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import norm
from virtual_ipm.utils.mathematics.random_sampling import InverseTransformSampling2D, \
    RejectionSampling2D


# Generate from Gaussian(0, 1) in 5 sigma range.

x_min, x_max = -5, 5
its = InverseTransformSampling2D(norm.pdf, np.linspace(x_min, x_max, 1000))
rs = RejectionSampling2D(norm.pdf, x_min, x_max, norm.pdf(0))

n_samples = 100000
t1 = clock()
its_samples = its.create_samples(n_samples)
print('[ITS] time: ', clock() - t1)
print('[ITS] mean, std: ', np.mean(its_samples), np.std(its_samples))
t1 = clock()
rs_samples = rs.create_samples(n_samples)
print('[RS] time: ', clock() - t1)
print('[RS] mean, std: ', np.mean(rs_samples), np.std(rs_samples))

bins = np.linspace(x_min, x_max, 100)
ref_prob = norm.pdf(bins)
its_hist = np.histogram(its_samples, bins=bins)
its_prob = its_hist[0].astype(float) / np.sum(its_hist[0]) * np.sum(ref_prob)
rs_hist = np.histogram(rs_samples, bins=bins)
rs_prob = rs_hist[0].astype(float) / np.sum(rs_hist[0]) * np.sum(ref_prob)

plt.figure()
plt.plot(its_hist[1][:-1], its_prob, '-o', label='ITS')
plt.plot(rs_hist[1][:-1], rs_prob, '-s', label='RS')
plt.plot(bins, ref_prob, '--x', label='REF')
plt.legend()
plt.show()
