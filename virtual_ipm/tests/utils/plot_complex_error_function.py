# -*- coding: utf-8 -*-

#    Virtual-IPM is a software for simulating IPMs and other related devices.
#    Copyright (C) 2017  The IPMSim collaboration <http://ipmsim.gitlab.io/IPMSim>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import matplotlib.pyplot as plt
import numpy
from scipy.special import erf as scipy_erf

try:
    from errffor import errf
except ImportError:
    print 'You need to compile the corresponding Fortran module first: `make errorfunction`'
    raise


reals = numpy.linspace(-10., 10., 100)
imags = numpy.linspace(-10., 10., 100)
zs = numpy.array(map(lambda x: complex(x[0], x[1]), zip(reals, imags)))

# Following equation (5) of M.Bassetti, G.A.Erskine: Closed Expression for the Electrical Field of a Two-Dimensional
#                                                    Gaussian Charge, CERN-ISR-TH/80-06
values_scipy = numpy.exp(-zs**2) * (1. + 1j * scipy_erf(zs))

values_fortran = map(lambda x: errf(x[0], x[1]), zip(zs.real, zs.imag))
values_fortran_real, values_fortran_imag = zip(*values_fortran)

plt.plot(reals, values_scipy.real, 'b-', label='scipy')
plt.plot(reals, values_fortran_real, 'r--', label='Fortran')
plt.axes().set_title('Real part')
plt.legend()

plt.figure()
plt.plot(reals, values_scipy.imag, 'b-', label='scipy')
plt.plot(reals, values_fortran_imag, 'r--', label='Fortran')
plt.axes().set_title('Imaginary part')
plt.legend()

plt.show()
