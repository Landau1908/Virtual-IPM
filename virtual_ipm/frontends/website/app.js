String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

String.prototype.interpolate = function (o) {
    return this.replace(/{([^{}]*)}/g,
        function (a, b) {
            let r = o[b];
            return typeof r === 'string' || typeof r === 'number' ? r : a;
        }
    );
};

inputs_per_type = {
    // 'string': '<input class="form-control" type="text">'
    BoolParameter: '<input type="checkbox">',
    IntegerParameter: '<input type="text" placeholder="{placeholder}">',
    NumberParameter: '<input type="text" placeholder="{placeholder}">',
    StringParameter: '<input type="text" placeholder="{placeholder}">',
    VectorParameter: '<input type="text" placeholder="{placeholder}">'
};

function load_specific_component(component) {
    if ("parameters" in component) {
        component["parameters"].forEach(function (parameter) {
            console.log("Appending input field for type " + parameter["type"]);
            let input_form = inputs_per_type[parameter["type"]];
            input_form = input_form.interpolate({placeholder: parameter["type"]});
            // $("#inner-container").append(inputs_per_type[parameter["type"]]);
            let name = parameter["path"].split("/").slice(-1).pop();
            $("#component-table").append(`<tr><td>${name}</td><td>${input_form}</td></tr>`);
        });
    }
}

function main () {
    const base_url = "http://127.0.0.1:5000";
    $.getJSON(base_url + "/components", null, function (components) {
        components["components"].forEach(function (component) {
            console.log("Requesting component " + component);
            $.getJSON(base_url + "/components/" + component, null, function (specifics) {
                if (Array.isArray(specifics[component])) {
                    // Contains a list of specific implementations.
                    console.log("Got a list of specific components");
                    specifics[component].forEach(function (specific_name) {
                        $.getJSON(base_url + "/components/" + component + "/" + specific_name, null, function (specific_component) {
                            $("#component-table").append('<tr><td><b>{component}</b></td></tr>'.interpolate({component: specific_name.capitalize()}));
                            load_specific_component(specific_component[specific_name]);
                        })
                    })
                } else {
                    // Represents already a specific implementation.
                    console.log("Got a specific component.");
                    $("#component-table").append('<tr><td><b>{component}</b></td></tr>'.interpolate({component: component.capitalize()}));
                    load_specific_component(specifics[component]);
                }
            });
        });
    });
}

$(document).ready(main);
